# Contributor: Dmitry Zakharchenko <dmitz@disroot.org>
# Maintainer: Dmitry Zakharchenko <dmitz@disroot.org>
pkgname=pdfcpu
pkgver=0.4.2
pkgrel=0
pkgdesc="PDF processor written in Go"
url="https://pdfcpu.io"
# s390x: fails tests
arch="all !s390x"
license="Apache-2.0"
makedepends="go"
checkdepends="tzdata"
source="$pkgname-$pkgver.tar.gz::https://github.com/pdfcpu/pdfcpu/archive/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o bin/pdfcpu ./cmd/...
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/pdfcpu "$pkgdir"/usr/bin/pdfcpu
}

sha512sums="
ab90f9ad59cb622b46851f8fd9e8920aa56302c313c961c31b1068865601bacc81498aa1b6678e02eeca42e0335c97d2736ae7b1415d00db063fdd57ec228803  pdfcpu-0.4.2.tar.gz
"
