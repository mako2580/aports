rebased config changes and paths to buildroot -lnl
cherry-pick from main: https://github.com/flutter/engine/pull/40980

From b5a8c75cfed7872553874b1721e82d3548f93b4d Mon Sep 17 00:00:00 2001
From: Chris Bracken <chris@bracken.jp>
Date: Thu, 6 Apr 2023 15:08:08 -0700
Subject: [PATCH] Support disabling backtrace support

This adds a gn flag (--backtrace, --no-backtrace) that defaults to
enabling backtraces, which drives a gn variable `enable_backtrace` which
is defaulted true for debug builds.

Backtrace collection is supported on Windows, and on POSIX-like
operating systems via execinfo.h. execinfo support exists in macOS/iOS,
and in glibc and uclibc on Linux. musl libc notably does not include
execinfo support, so this provides an escape hatch to build with
backtrace_stub.cc for situations in which compile time support doesn't
exist.

Proposed as an alternative to
https://github.com/flutter/engine/pull/40958 by @selfisekai.

Issue: https://github.com/flutter/flutter/issues/124285
---
 common/config.gni |  3 +++
 fml/BUILD.gn      |  4 ++--
 tools/gn          | 20 ++++++++++++++++++++
 3 files changed, 25 insertions(+), 2 deletions(-)

diff --git a/flutter/common/config.gni b/flutter/common/config.gni
index 142701b8542f..1e22be21da6b 100644
--- ./flutter/common/config.gni.orig
+++ ./flutter/common/config.gni
@@ -19,6 +19,9 @@
 
   # Whether to build host-side development artifacts.
   flutter_build_engine_artifacts = true
+
+  # Whether to include backtrace support.
+  enable_backtrace = true
 }
 
 # feature_defines_list ---------------------------------------------------------
diff --git a/flutter/fml/BUILD.gn b/flutter/fml/BUILD.gn
index 1656b1144609..9f7310b5b065 100644
--- a/flutter/fml/BUILD.gn
+++ b/flutter/fml/BUILD.gn
@@ -97,7 +97,7 @@ source_set("fml") {
     "wakeable.h",
   ]
 
-  if (is_mac || is_linux || is_win || (is_ios && is_debug)) {
+  if (enable_backtrace) {
     sources += [ "backtrace.cc" ]
   } else {
     sources += [ "backtrace_stub.cc" ]
@@ -116,7 +116,7 @@ source_set("fml") {
     "//third_party/icu",
   ]
 
-  if (is_mac || is_linux || is_win || (is_ios && is_debug)) {
+  if (enable_backtrace) {
     # This abseil dependency is only used by backtrace.cc.
     deps += [ "//third_party/abseil-cpp/absl/debugging:symbolize" ]
   }
diff --git a/tools/gn b/tools/gn
index 366ec6bdd3ad..1c13e7bbcab1 100755
--- a/flutter/tools/gn
+++ b/flutter/tools/gn
@@ -393,6 +393,15 @@ def to_gn_args(args):
   # flags allow preventing those targets from being part of the build tree.
   gn_args['enable_desktop_embeddings'] = not args.disable_desktop_embeddings
 
+  # Determine whether backtace support should be compiled in.
+  if args.backtrace:
+    gn_args['enable_backtrace'] = (
+        args.target_os in ['linux', 'mac', 'win'] or
+        args.target_os == 'ios' and runtime_mode == 'debug'
+    )
+  else:
+    gn_args['enable_backtrace'] = False
+
   # Overrides whether Boring SSL is compiled with system as. Only meaningful
   # on Android.
   gn_args['bssl_use_clang_integrated_as'] = True
@@ -773,6 +782,17 @@ def parse_args(args):
       '--arm-float-abi', type=str, choices=['hard', 'soft', 'softfp']
   )
 
+  # Whether to compile in backtrace support.
+  # Available for Windows and POSIX platforms whose libc includes execinfo.h.
+  # MUSL doesn't include execinfo.h should be build with --no-backtrace.
+  parser.add_argument(
+      '--backtrace',
+      default=True,
+      action='store_true',
+      help='Whether OS support exists for collecting backtraces.'
+  )
+  parser.add_argument('--no-backtrace', dest='backtrace', action='store_false')
+
   parser.add_argument(
       '--build-engine-artifacts',
       default=True,
