# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=py3-srsly
pkgver=2.4.7
pkgrel=0
pkgdesc="Modern high-performance serialization utilities for Python"
url="https://github.com/explosion/srsly"
arch="all"
license="MIT"
depends="python3 py3-numpy py3-tz"
options="!check" # fail to find self for some reason
makedepends="python3-dev
	     py3-setuptools
	     cython
	     py3-wheel
	     py3-gpep517
	     py3-installer"
checkdepends="py3-pytest py3-pytest-timeout py3-mock"
subpackages="$pkgname-doc $pkgname-pyc"
source="srsly-$pkgver.tar.gz::https://github.com/explosion/srsly/archive/v$pkgver.tar.gz"
builddir="$srcdir/srsly-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -vv
}

package() {
	python -m installer -d "$pkgdir" .dist/*.whl

	install -Dm644 README.md "$pkgdir/usr/share/doc/$pkgname/README.md"

	rm -r "$pkgdir"/usr/lib/python3*/site-packages/srsly/tests
}

sha512sums="
4a0296f1d8d87fe9a3e832555a818dd18939560ab729f78144febb071a5f2c44859469e4b5e09a44042819d9ca94a2765317f3bef6042156a27227a7159680b9  srsly-2.4.7.tar.gz
"
