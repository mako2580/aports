# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer:
pkgname=py3-gst
pkgver=1.22.5
pkgrel=1
pkgdesc="GStreamer Python3 bindings"
url="https://cgit.freedesktop.org/gstreamer/gst-python/"
arch="all"
license="LGPL-2.1-or-later"
depends="py3-gobject3 gst-plugins-base"
makedepends="
	gst-plugins-base-dev
	gstreamer-dev
	meson
	py3-gobject3-dev
	python3-dev
	"
source="https://gstreamer.freedesktop.org/src/gst-python/gst-python-$pkgver.tar.xz
	suffix.patch
	"
builddir="$srcdir/gst-python-$pkgver"

prepare() {
	default_prepare

	local pyso="$(readlink /usr/lib/libpython*.*.so)"
	[ -n "$pyso" ]
	msg "libpython: $pyso"
	local suff="${pyso#libpython*.so.}"
	sed -i "s|@SUFF@|$suff|" meson.build
}

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Dpython=/usr/bin/python3 \
		. output
	meson compile -C output
}

check() {
	meson test --print-errorlogs --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
0f976cbc8c394b7eea9b51fbfdc6f10ff95c270cdcc4006a244e6770b69048ff1b13a2f2499ad8d88f3e74ab181fac65cadabb01e9fcd29a082a93580f788e36  gst-python-1.22.5.tar.xz
62e54d028898ff15d51d3fa863f419a533a05bb510b6c391778d2fff5e245143771b38902deb512ac5a7a7d704d7fc9f4e84d22a4761eb85e535b67d0ab640f4  suffix.patch
"
