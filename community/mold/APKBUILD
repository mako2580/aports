# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=mold
pkgver=2.0.0
pkgrel=3
pkgdesc="fast modern linker"
url="https://github.com/rui314/mold"
arch="all"
license="MIT"
makedepends="
	clang
	cmake
	linux-headers
	llvm-dev
	mimalloc2-dev
	onetbb-dev
	openssl-dev
	samurai
	xxhash-dev
	zlib-dev
	zstd-dev
	"
checkdepends="
	bash
	dwarf-tools
	grep
	perl
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rui314/mold/archive/refs/tags/v$pkgver.tar.gz
	0001-Weak-undefs-should-not-keep-DSOs-alive.patch
	xxhash.patch
	"

case "$CARCH" in
s390x)
	# copyrel, shared-abs-sym, tls-large-alignment, tls-small-alignment
	options="!check"
	;;
esac
case "$CARCH" in
s390x|riscv64)
	;;
*)
	makedepends="$makedepends lld"
	export LDFLAGS="$LDFLAGS -fuse-ld=lld"
	;;
esac

build() {
	CC=clang CXX=clang++ \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_DISABLE_PRECOMPILE_HEADERS=ON \
		-DMOLD_LTO=ON \
		-DMOLD_USE_SYSTEM_MIMALLOC=ON \
		-DMOLD_USE_SYSTEM_TBB=ON \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)"

	cmake --build build
}

check() {
	ctest --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f352044226a21edff902cbbd3cb1e763ba1fef81b3cd850f29b267405ef17303b702bf086ed4e2a4bb07fdc2af4ef4d32e5a589d62b5375e86174e2743a6f1aa  0001-Weak-undefs-should-not-keep-DSOs-alive.patch
11c3d1e07fe4fcc28cff58b8e432526e4604aa55b49fa47c9495e439206fb9f6a1686b1c5bd2dc907ffd7fe62ac1c72317619fc8a5d7caaa5c327de585f16827  mold-2.0.0.tar.gz
9a54c572df99c79e501806ad08cf5e0f5ef7a91f02c973c6e9a86980b1e1fadb0f028746f160bcf89933453a1854e481136ee2fbfb2dbde7f0f53b16e215bd71  xxhash.patch
"
