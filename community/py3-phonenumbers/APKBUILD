# Contributor: Kaarle Ritvanen <kunkku@alpinelinux.org>
# Maintainer: Kaarle Ritvanen <kunkku@alpinelinux.org>
pkgname=py3-phonenumbers
pkgver=8.13.17
pkgrel=0
pkgdesc="International phone number library for Python"
url="https://github.com/daviddrysdale/python-phonenumbers"
arch="noarch"
license="Apache-2.0"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/phonenumbers/phonenumbers-$pkgver.tar.gz"
builddir="$srcdir/phonenumbers-$pkgver"

replaces="py-phonenumbers" # Backwards compatibility
provides="py-phonenumbers=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 testwrapper.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d42da37b624fdff812ee4d34eda3166a8bc363776937833194c95ad78a5d3c089b74339590b72237febcf6671dbafaa35a2d09be545c68d6102f6d40d5961523  phonenumbers-8.13.17.tar.gz
"
