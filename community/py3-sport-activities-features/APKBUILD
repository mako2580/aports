# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-sport-activities-features
pkgver=0.3.14
pkgrel=0
pkgdesc="A minimalistic toolbox for extracting features from sport activity files"
url="https://github.com/firefly-cpp/sport-activities-features"
arch="noarch !ppc64le !s390x !riscv64" # py3-niaaml
license="MIT"
depends="
	python3
	py3-dotmap
	py3-geopy
	py3-geotiler
	py3-gpxpy
	py3-matplotlib
	py3-niaaml
	py3-overpy
	py3-requests
	py3-tcx2gpx
	py3-tcxreader
	"
checkdepends="py3-pytest"
makedepends="py3-gpep517 py3-poetry-core"
subpackages="$pkgname-doc $pkgname-pyc"
source="https://github.com/firefly-cpp/sport-activities-features/archive/$pkgver/sport-activities-features-$pkgver.tar.gz"
builddir="$srcdir/sport-activities-features-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	# exclude network tests
	python3 -m pytest -k "not test_weather and not overpy_node_manipulation and not test_data_analysis"
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/sport_activities_features-$pkgver-py3-none-any.whl

	install -Dm644 docs/preprints/A_minimalistic_toolbox.pdf -t "$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
cf42aaa71a84473f87f0fbb7783b043e59c6bc0bc947ea133a2d75e1eb59f697dd75d09742c37fbdc6eca47bcc69ded786a651253b500b551b29d5da66fdac0d  sport-activities-features-0.3.14.tar.gz
"
