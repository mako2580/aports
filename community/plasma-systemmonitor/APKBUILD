# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-systemmonitor
pkgver=5.27.7
pkgrel=1
pkgdesc="An application for monitoring system resources"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LicenseRef-KDE-Accepted-GPL AND LicenseRef-KDE-Accepted-LGPL AND CC0-1.0"
depends="
	kirigami2
	ksystemstats
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	knewstuff-dev
	kservice-dev
	libksysguard-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-systemmonitor.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-systemmonitor-$pkgver.tar.xz"
subpackages="$pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-systemmonitor.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# ksystemstatstest is broken
	ctest --test-dir build --output-on-failure -E "ksystemstatstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
935c73a0a0705586a408634f55e63401ffa80e11e3e0b0b1291ed1a64944e6dd21d031206e9a6afad37a24b98f633ad748cce8e048689decacadf47067987930  plasma-systemmonitor-5.27.7.tar.xz
"
