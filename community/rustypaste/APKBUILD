# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=rustypaste
pkgver=0.12.0
pkgrel=0
pkgdesc="Minimal file upload/pastebin service"
url="https://github.com/orhun/rustypaste"
# s390x, ppc64le, riscv64: blocked by ring crate
# x86: test failure
arch="all !s390x !ppc64le !riscv64 !x86"
license="MIT"
makedepends="cargo zstd-dev openssl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/rustypaste/archive/v$pkgver.tar.gz"
options="net"

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libzstd.
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		zstd = { rustc-link-lib = ["zstd"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --no-default-features --features openssl
}

check() {
	cargo test --frozen -- --test-threads 1
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 config.toml -t "$pkgdir/etc/rustypaste"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}

sha512sums="
0043e5ef1f88c700e3befa13bef83d4e80281c122b80f4e3ec063004d3938d17de277d0c35369f0960c56260da65e2b7efc9aa5adca7ffb13985b6a6bd08adaf  rustypaste-0.12.0.tar.gz
"
