# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=print-manager
pkgver=23.04.3
pkgrel=1
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://www.kde.org/applications/utilities/"
pkgdesc="A tool for managing print jobs and printers"
license="GPL-2.0-or-later"
makedepends="
	cups-dev
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/print-manager-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8462e241f73259f0cfca927f33d778df0d83d3db8cd5347dea8cd48973774352bab5d1dd2352ed4d8cd908ab6392189ec0e6468b679cfaf85afae1c574a2a3e6  print-manager-23.04.3.tar.xz
"
