# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kclock
pkgver=23.04.3
pkgrel=2
pkgdesc="Clock app for Plasma Mobile"
url="https://invent.kde.org/utilities/kclock"
# armhf blocked by qt5-qtdeclarative
# x86 broken
arch="all !armhf !x86"
license="LicenseRef-KDE-Accepted-GPL"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	knotifications-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kclock-$pkgver.tar.xz"
options="!check" # No tests

_commit=""
_repo_url="https://invent.kde.org/utilities/kclock.git"
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8e2fa87aa02a5ec71a847a111e79a833c0b6b490831cacff2b1734071bdbad819c4dcede2416f6a56c265cce41eea6174a52ca1465d7c3dd2c9e6485a34e0a93  kclock-23.04.3.tar.xz
"
