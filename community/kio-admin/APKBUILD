# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kio-admin
pkgver=23.04.3
pkgrel=1
pkgdesc="Manage files as administrator using the admin:// KIO protocol"
url="https://invent.kde.org/system/kio-admin"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND (GPL-2.0-only OR GPL-3.0-only)"
# zstd is purely used to unpack the source archive
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kio-admin-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
80fb6950a8f27f0271afbc202be784ebbac0d2ca3ff3695f52edd80af07f17f1c27f8b435003363e272f1832aeb9483efbe42e372434da8b2b549ad8e7fba134  kio-admin-23.04.3.tar.xz
"
