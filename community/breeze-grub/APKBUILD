# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=breeze-grub
pkgver=5.27.7
pkgrel=1
pkgdesc="Breeze theme for GRUB"
arch="noarch !s390x !armhf" # armhf blocked by extra-cmake-modules
url="https://kde.org/plasma-desktop/"
license="GPL-3.0-or-later"
depends="grub"
makedepends="
	extra-cmake-modules
	font-unifont
	grub-mkfont
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/breeze-grub.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-grub-$pkgver.tar.xz"
options="!check" # No test suite available

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/breeze-grub.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	./mkfont.sh
}

package() {
	install -d "$pkgdir"/usr/share/grub/themes
	cp -r breeze "$pkgdir"/usr/share/grub/themes
}
sha512sums="
d0c8218795f3512cb147917b7a437419ab66d6856fb8116dae2e6c7b35b3def0dd619901645795bbd9bd2afd3475a608a08d8e40e9d341d7b2ed4602d3252a4a  breeze-grub-5.27.7.tar.xz
"
