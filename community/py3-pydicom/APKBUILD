# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-pydicom
pkgver=2.4.2
pkgrel=0
pkgdesc="Read, modify and write DICOM files with python"
url="https://github.com/pydicom/pydicom"
arch="noarch"
license="MIT AND BSD-3-Clause"
depends="python3 py3-numpy"
makedepends="
	py3-gpep517
	py3-flit-core
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/pydicom/pydicom/archive/v$pkgver/pydicom-$pkgver.tar.gz"
builddir="$srcdir/pydicom-$pkgver"
options="!check" # do not test for now | collection is very time consuming

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -W ignore::DeprecationWarning
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/pydicom/tests

	rm -r "$pkgdir"/usr/lib/python3.*/site-packages/pydicom/data/test_files
}

sha512sums="
dfe1e4a1d674708d008182eb41dd3bdce128ab9789cdef09f4ce02e17a38e788af676dea581c08dba424f92ffc09376a50bc10e94e35ad348a34aefa6abecfb0  pydicom-2.4.2.tar.gz
"
