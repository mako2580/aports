# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=libksysguard
pkgver=5.27.7
pkgrel=1
pkgdesc="KDE system monitor library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	kauth-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libcap-dev
	libnl3-dev
	libpcap-dev
	lm-sensors-dev
	plasma-framework-dev
	qt5-qttools-dev
	qt5-qtwebchannel-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/libksysguard.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/libksysguard-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

replaces="ksysguard<5.22"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/libksysguard.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# processtest requires working OpenGL
	xvfb-run ctest --test-dir build --output-on-failure -E "processtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
928afc2dd38dac8e8b94d2293eb7121ca9fa60f7707b969977a56d98db833af6dd1f26d7f1f726fd5c2b3f328d9357ce93751db735668d8b31002a8e476d6627  libksysguard-5.27.7.tar.xz
"
