# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=picard
pkgver=2.9
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://picard.musicbrainz.org/"
pkgdesc="Official MusicBrainz tagger"
license="GPL-2.0-or-later"
depends="
	chromaprint
	py3-dateutil
	py3-fasteners
	py3-mutagen
	py3-qt5
	py3-yaml
	"
makedepends="
	gettext
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest"
subpackages="$pkgname-lang $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/metabrainz/picard/archive/release-$pkgver.tar.gz"
builddir="$srcdir/picard-release-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
837f70b239acc0d2846be79fb225e5bc7a3b7c3d2e928c34bd01730b6049809a58c274a4e49c5cc42f253f5c113c19b331ac1489427f8dbb3c31515d2511e4dc  picard-2.9.tar.gz
"
