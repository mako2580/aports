# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kwrited
pkgver=5.27.7
pkgrel=1
pkgdesc="KDE daemon listening for wall and write messages"
arch="all !armhf" # qt5-qtdeclarative-dev  unavilable on armhf
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	knotifications-dev
	kpty-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kwrited.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kwrited-$pkgver.tar.xz"
options="!check" # No tests available

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/kwrited.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
61597f45e942a3e7ff5288303e6b8effcc31a8da0a4bdbb31bc17d66cb9f7c1d94fcf9d555dfe5f6b2a217d8b893f8a1f04a7c7d424e7f0747f8416e0c461053  kwrited-5.27.7.tar.xz
"
