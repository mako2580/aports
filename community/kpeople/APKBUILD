# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kpeople
pkgver=5.108.0
pkgrel=2
pkgdesc="A library that provides access to all contacts and the people who hold them"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends="qt5-qtbase-sqlite"
depends_dev="
	kcoreaddons-dev
	ki18n-dev
	kitemviews-dev
	kservice-dev
	kwidgetsaddons-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kpeople.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpeople-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kpeople.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# personsmodeltest fails
	ctest --test-dir build --output-on-failure -E '(personsmodeltest)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5e9b74bb15d7022fb42712aa185feb14ed5c5729bb1573beede025126cb599dd0d444c466a258a814312bbc46d6ecf96ac3697f6f52e20ce628f2de6b5ee1332  kpeople-5.108.0.tar.xz
"
