# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-other
pkgname=ktextaddons
pkgver=1.4.0
pkgrel=0
pkgdesc="Various text handling addons"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/libraries/ktextaddons/"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends_dev="
	karchive-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qtkeychain-dev
	"
makedepends="
	$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/ktextaddons/ktextaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	local skipped_tests="("
	local tests="
		languagetoolconfigwidget
		translator-translatorwidget
		translator-translatorengineloader
		grammaleceresultwidget
		grammaleceresultjob
		grammalececonfigwidget
	"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	xvfb-run ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
8971d19da9dc597b4cfbbce31d42d4c7c100143569a04e5e04aa6f68514563c5422825ff35deb9b4dee86edb10f21c661e13b4cd7a0c753cd670c47c8f32911f  ktextaddons-1.4.0.tar.xz
"
